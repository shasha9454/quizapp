import 'package:flutter/material.dart';
import 'package:flutter/widgets.dart';
import 'package:quizapp/summary_item.dart';

class QuestionSUmmary extends StatelessWidget {
  const QuestionSUmmary(this.summaryData, {super.key});

  final List<Map<String, Object>> summaryData;
  @override
  Widget build(BuildContext context) {
    return SizedBox(
      height: 250,
      child: SingleChildScrollView(
        child: Column(
          children: summaryData.map(
            (data) {
              return SummaryItems(data);
            },
          ).toList(),
        ),
      ),
    );
  }
}
