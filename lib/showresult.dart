import 'package:flutter/material.dart';
import 'package:flutter/widgets.dart';
import 'package:google_fonts/google_fonts.dart';
import 'package:quizapp/data/questions.dart';
import 'package:quizapp/questionsummar.dart';

class ShowResult extends StatelessWidget {
  const ShowResult(
      {super.key, required this.chooseAnswer, required this.restartQuiz});
  final List<String> chooseAnswer;
  final void Function() restartQuiz;

  List<Map<String, Object>> get getSummaryData {
    final List<Map<String, Object>> summary = [];

    for (int i = 0; i < chooseAnswer.length; i++) {
      summary.add(
        {
          'question-index': i,
          'question': questions[i].text,
          'correct-answer': questions[i].answer[0],
          'user_answer': chooseAnswer[i],
        },
      );
    }
    return summary;
  }

  @override
  Widget build(BuildContext context) {
    final getAnswer = getSummaryData;
    final numTotalquestion = questions.length;
    final correctAnswer = getAnswer.where((text) {
      return text['user_answer'] == text['correct-answer'];
    }).length;

    return SizedBox(
      width: double.infinity,
      child: Container(
        margin: const EdgeInsets.all(55),
        child: Column(
          mainAxisAlignment: MainAxisAlignment.center,
          crossAxisAlignment: CrossAxisAlignment.stretch,
          children: [
            Text(
              "You answered $correctAnswer out of $numTotalquestion question correctly!",
              style: GoogleFonts.lato(
                  fontSize: 24,
                  color: const Color.fromARGB(255, 230, 200, 253),
                  fontWeight: FontWeight.bold),
              textAlign: TextAlign.center,
            ),

            QuestionSUmmary(getAnswer),
            const SizedBox(
              height: 10,
            ),
            // const Text(
            //   "Show result ....",
            //   style: TextStyle(fontSize: 24),
            // ),
            const SizedBox(
              height: 7,
            ),
            TextButton.icon(
              onPressed: restartQuiz,
              style: TextButton.styleFrom(
                  foregroundColor: Colors.black,
                  backgroundColor: Colors.amberAccent),
              icon: const Icon(Icons.refresh),
              label: const Text("Restart Quiz!"),
            )
          ],
        ),
      ),
    );
  }
}
