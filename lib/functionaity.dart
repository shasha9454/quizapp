import 'package:flutter/material.dart';
import 'package:quizapp/data/questions.dart';
import 'package:quizapp/showresult.dart';
import 'package:quizapp/start_screen.dart';
import 'package:quizapp/question.dart';

// ignore: must_be_immutable
class Functionality extends StatefulWidget {
  const Functionality({super.key});

  @override
  State<Functionality> createState() {
    return _FunctionalityState();
  }
}

class _FunctionalityState extends State<Functionality> {
  var changeWidget = 'firstWidget';
  List<String> getAnswer = [];
  void onStart() {
    setState(() {
      getAnswer = [];
      changeWidget = 'endWidget';
    });
  }

  void getResponse(String answer) {
    getAnswer.add(answer);
    if (getAnswer.length == questions.length) {
      setState(() {
        changeWidget = 'finalWidget';
      });
    }
  }

  void screenChange() {
    setState(() {
      getAnswer = [];
      changeWidget = 'endWidget';
    });
  }

  // @override
  // void initState() {
  //   activeState = StartScreen(screenChange);
  //   super.initState();
  // }
  @override
  Widget build(context) {
    Widget setWidget = StartScreen(screenChange);
    if (changeWidget == 'endWidget') {
      setWidget = Question(
        getresponse: getResponse,
      );
    }
    if (changeWidget == 'finalWidget') {
      setWidget = ShowResult(
        chooseAnswer: getAnswer,
        restartQuiz: onStart,
      );
    }

    return MaterialApp(
      home: Scaffold(
        body: Container(
          decoration: const BoxDecoration(
            gradient: LinearGradient(
              colors: [
                Colors.deepPurple,
                Colors.amberAccent,
              ],
              begin: Alignment.topLeft,
              end: Alignment.bottomRight,
            ),
          ),
          // child: (changeWidget == 'firstWidget')
          //     ? StartScreen(screenChange)
          //     : const Question(),
          child: setWidget,
        ),
      ),
    );
  }
}
