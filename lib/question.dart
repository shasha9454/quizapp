import 'package:flutter/material.dart';

import 'package:quizapp/answer.dart';
import 'package:quizapp/data/questions.dart';
import 'package:google_fonts/google_fonts.dart';

// ignore: must_be_immutable
class Question extends StatefulWidget {
  const Question({required this.getresponse, super.key});
  final void Function(String answer) getresponse;
  @override
  State<Question> createState() {
    return _QuestionState();
  }
}

class _QuestionState extends State<Question> {
  var currentQuestionIndex = 0;
  void answerQuestion(String answer) {
    widget.getresponse(answer);
    setState(() {
      currentQuestionIndex += 1;
    });
  }

  @override
  Widget build(BuildContext context) {
    final changeActivity = questions[currentQuestionIndex];
    return SizedBox(
      width: double.infinity,
      child: Container(
        margin: const EdgeInsets.all(35),
        child: Column(
          mainAxisAlignment: MainAxisAlignment.center,
          crossAxisAlignment: CrossAxisAlignment.stretch,
          children: [
            Text(
              changeActivity.text,
              style: GoogleFonts.lato(
                color: Colors.black,
                fontSize: 24,
                fontWeight: FontWeight.bold,
              ),
              textAlign: TextAlign.center,
            ),
            const Padding(padding: EdgeInsets.only(top: 28)),

            ...changeActivity.getShuffledMethod().map((text) {
              return AnswerButton(
                answerCreated: text,
                onTap: () {
                  answerQuestion(text);
                },
              );
            }),
            // AnswerButton(changeActivity.answer[0], () {}),
            // AnswerButton(changeActivity.answer[1], () {}),
            // AnswerButton(changeActivity.answer[2], () {}),
            // AnswerButton(changeActivity.answer[3], () {}),
          ],
        ),
      ),
    );
  }
}
