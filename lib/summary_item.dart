import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'package:flutter/widgets.dart';
import 'package:google_fonts/google_fonts.dart';

import 'package:quizapp/question_Identifier.dart';

class SummaryItems extends StatelessWidget {
  const SummaryItems(this.ItemData, {super.key});
  // ignore: non_constant_identifier_names
  final Map<String, Object> ItemData;

  @override
  Widget build(BuildContext context) {
    final isCorrectAnswer =
        ItemData['user_answer'] == ItemData['correct-answer'];

    return Row(
      crossAxisAlignment: CrossAxisAlignment.center,
      children: [
        QuestionIdentifier(
            questionIndex: ItemData['question-index'] as int,
            isCorrectAnswer: isCorrectAnswer),
        const SizedBox(
          height: 35,
        ),
        Expanded(
            child: Column(
          crossAxisAlignment: CrossAxisAlignment.start,
          children: [
            const SizedBox(
              height: 15,
            ),
            Text(
              ItemData['question'] as String,
              style: GoogleFonts.lato(
                fontWeight: FontWeight.bold,
              ),
            ),
          ],
        ))
      ],
    );
  }
}
