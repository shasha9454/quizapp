class QuizQuestion {
  const QuizQuestion(this.text, this.answer);
  final String text;
  final List<String> answer;

  getShuffledMethod() {
    final checkShuffled = List.of(answer);
    checkShuffled.shuffle();
    return checkShuffled;
  }
}
