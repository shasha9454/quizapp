import 'package:quizapp/models/quiz_question.dart';

const questions = [
  QuizQuestion(
      'The data structure required to check whether an expression contains a balanced parenthesis is?',
      [
        'Stack',
        'Queue',
        'Tree',
        "Array",
      ]),
  QuizQuestion(
    'What are the main buiding blocks of flutter UIs?',
    [
      'Widget',
      'Components',
      'Blocks',
      'Functions',
    ],
  ),
  QuizQuestion('What is the data structure?', [
    'A way to store and organize data',
    'A Programming Language',
    'A collection of algorithms',
    'A type of computer hardware',
  ]),
  QuizQuestion('Which data structure is used for implementing recursion?',
      ['Stack', 'Queue', 'List', 'Array']),
  QuizQuestion(
      'The data structure required for Breadth frst Traversal on a graph is?', [
    'Queue',
    'Array',
    'Stack',
    'Tree',
  ])
];
