import 'package:flutter/material.dart';

// ignore: must_be_immutable
class StartScreen extends StatelessWidget {
  StartScreen(this.currentState, {super.key});

  void Function() currentState;

  @override
  Widget build(context) {
    return Center(
      child: Column(
        mainAxisSize: MainAxisSize.min,
        children: [
          const Opacity(
            opacity: 0.5,
          ),
          Image.asset(
            'lib/asset/ideas.png',
            width: 300,
          ),
          const SizedBox(height: 85),
          const Text(
            'Learn Flutter the fun Way! ',
            style: TextStyle(color: Colors.white, fontSize: 24),
          ),
          const SizedBox(
            height: 25,
          ),
          OutlinedButton.icon(
              icon: const Icon(Icons.arrow_right_alt),
              onPressed: currentState,
              label: const Text(
                'Start Quiz',
                style: TextStyle(
                  fontSize: 28,
                  color: Colors.white60,
                ),
              )),
        ],
      ),
    );
  }
}
